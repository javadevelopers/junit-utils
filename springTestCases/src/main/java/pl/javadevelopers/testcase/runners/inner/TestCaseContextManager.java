package pl.javadevelopers.testcase.runners.inner;

import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestContextManager;

/**
 * Created by AdrianS on 2014-05-15.
 */
public class TestCaseContextManager extends TestContextManager {
    public TestCaseContextManager(Class<?> testClass) {
        super(testClass);
    }

    public TestCaseContextManager(Class<?> testClass, String defaultContextLoaderClassName) {
        super(testClass, defaultContextLoaderClassName);
    }

    public TestContext getContext() {
        return this.getTestContext();
    }
}
