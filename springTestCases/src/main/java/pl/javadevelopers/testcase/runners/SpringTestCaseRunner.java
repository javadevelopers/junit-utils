package pl.javadevelopers.testcase.runners;

import org.junit.runner.RunWith;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.springframework.context.ApplicationContext;
import pl.hiber.testcase.runners.TestCaseRunner;
import pl.hiber.testcase.runners.inner.InnerTestCaseRunner;
import pl.javadevelopers.testcase.runners.inner.SpringInnerTestCaseRunner;
import pl.javadevelopers.testcase.runners.inner.TestCaseContextManager;

/**
 * Created by AdrianS on 2014-05-14.
 */
public class SpringTestCaseRunner extends TestCaseRunner {

    private TestCaseContextManager testContextManager;
    /**
     * Constructor for run all test case's in test class
     *
     * @param type
     */
    public SpringTestCaseRunner(Class<?> type) throws InitializationError {
        super(type);
    }

    @Override
    protected Object createTestInstance() throws Exception {
        testContextManager = createTestContextManager(getTestClass().getJavaClass());
        Object testInstance = super.createTestInstance();
        getTestContextManager().prepareTestInstance(testInstance);
        ApplicationContext appContext = testContextManager.getContext().getApplicationContext();
        //appContext.getParentBeanFactory().
        return testInstance;
    }

    protected TestCaseContextManager createTestContextManager(Class<?> clazz)
    {
        return new TestCaseContextManager(clazz, null);
    }

    protected final TestCaseContextManager getTestContextManager()
    {
        return this.testContextManager;
    }

    @Override
    protected InnerTestCaseRunner prepareInnerTestCaseRunner(Class<?> type, FrameworkMethod frameworkMethod) throws InitializationError {
        SpringInnerTestCaseRunner springTestCaseRunner = new SpringInnerTestCaseRunner(type, frameworkMethod, getTestInstance(), getOtherTestInstances(), getTestContextManager());
        springTestCaseRunner.buildActionsList();
        return springTestCaseRunner;
    }

    /** Test class constructor validation */
    @Override
    protected void validateConstructorAndAnnotations(Class<?> type) throws InitializationError {
        if (type.getConstructors().length != 1) {
            throw new InitializationError("Test class " + type.getName() + " must have only one public constructor");
        }
        RunWith runWith = type.getAnnotation(RunWith.class);
        if (runWith != null && runWith.value() != SpringTestCaseRunner.class) {
            throw new InitializationError("Test class " + type.getName() + " must be annotated with @RunWith(TestCaseRunner.class)");
        }
    }
}
