package pl.javadevelopers.testcase.runners.inner;

import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.springframework.context.ApplicationContext;
import pl.hiber.testcase.runners.inner.InnerTestCaseRunner;

import java.util.Map;

/**
 * Created 2014 by 4ib3r
 * Runner for single test case.
 */
public class SpringInnerTestCaseRunner extends InnerTestCaseRunner {

    private TestCaseContextManager testContextManager;
    private ApplicationContext applicationContext;
    /**
     * Constructor that's allow to create runner for simple test case
     *  @param testClass
     * @param frameworkMethod
     * @param testInstance
     * @param otherTestInstances
     * @param testContextManager
     */
    public SpringInnerTestCaseRunner(Class<?> testClass, FrameworkMethod frameworkMethod, Object testInstance, Map<String, Object> otherTestInstances, TestCaseContextManager testContextManager) throws InitializationError {
        super(testClass, frameworkMethod, testInstance, otherTestInstances);
        this.testContextManager = testContextManager;
        this.applicationContext = testContextManager.getContext().getApplicationContext();
    }

    /** Creation new instance of test class or pojo, or get it from list if it exist. */
    @Override
    protected Object createOrGetTestInstance(Class<?> type) throws IllegalAccessException, InstantiationException {
        if(type == getTestClass().getJavaClass()) {
            return testInstance;
        }
        Object testInstance = applicationContext.getBean(type);
        if (testInstance == null)
            testInstance = testInstances.get(type.getName());
        else throw new InstantiationException("Can't find bean of type: " + type.getName());
        /*if (testInstance == null) {
            testInstance = type.newInstance();
            testInstances.put(type.getName(), testInstance);
        }*/
        return testInstance;
    }
}