package pl.javadevelopers.spring.test.beans;

import org.springframework.stereotype.Component;

/**
 * Created by AdrianS on 2014-05-19.
 */
@Component
public class SpringTestBean {
    private int testInt;

    public int getTestInt() {
        return testInt;
    }

    public void setTestInt(int testInt) {
        this.testInt = testInt;
    }
}
