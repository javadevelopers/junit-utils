import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import pl.hiber.testcase.annotations.TestCase;
import pl.javadevelopers.spring.test.beans.SpringTestBean;
import pl.javadevelopers.testcase.annotations.Spy;
import pl.javadevelopers.testcase.runners.SpringTestCaseRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by AdrianS on 2014-05-15.
 */
@RunWith(SpringTestCaseRunner.class)
@ContextConfiguration("classpath*:spring.xml")
public class SpringTestCaseTest {
    @Autowired @Spy
    private SpringTestBean springTestBean;

    @TestCase
    public void testCase(SpringTestCaseTest testCaseTest, SpringTestBean springTestBean) {
        springTestBean.setTestInt(5);
        testCaseTest.test1();
    }

    @Test
    public void test1() {
        assertEquals(springTestBean.getTestInt(), 5);
        System.out.println("SpringTestCaseTest.test1");
    }
}
