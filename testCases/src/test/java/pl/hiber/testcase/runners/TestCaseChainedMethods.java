package pl.hiber.testcase.runners;

import org.junit.runner.RunWith;
import pl.hiber.testcase.annotations.TestCase;
import pl.hiber.testcase.runners.helpers.TestObj;

/**
 * Created by 4ib3r on 2014-07-02.
 */
@RunWith(TestCaseRunner.class)
public class TestCaseChainedMethods {

    @TestCase
    public void testCase(TestObj testObj) {
        testObj.action();
        testObj.testObj(2).action();
        testObj.testObj(2).action(4);
    }

    @TestCase
    public void testCase2(GenericTests genericTests) {
        genericTests.genericTest();
    }
}
