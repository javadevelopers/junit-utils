package pl.hiber.testcase.runners.helpers;

/**
 * Created by 4ib3r on 2014-07-02.
 * Object for tests
 */
public class TestObj {
    private int number = 0;
    public TestObj testObj(int number) {
        TestObj t = new TestObj();
        t.number = number;
        return t;
    }

    public void action() {
        System.out.println("action" + number);
    }

    public void action(int num) {
        System.out.println("action" + number + "_" + num);
    }
}
