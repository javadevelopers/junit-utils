package pl.hiber.testcase.runners.helpers;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by AdrianS on 24.09.14.
 */
public class CartSystem {
    private float rabat = 0;
    private Set<String> products = new LinkedHashSet<>();


    public void setRabat(float rabat) {
        this.rabat = rabat;
    }

    public float getRabat() {
        return rabat;
    }

    public void addProduct(String ean) {
        products.add(ean);
    }

    public int getProductsCount() {
        return products.size();
    }

    public void finishCart() {

    }


}
