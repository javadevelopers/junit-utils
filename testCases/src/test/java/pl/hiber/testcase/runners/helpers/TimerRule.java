package pl.hiber.testcase.runners.helpers;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by AdrianS on 2014-07-03.
 */
public class TimerRule extends TestWatcher {
    Map<Integer, Long> timeMap = new HashMap<>();
    @Override
    protected void starting(Description description) {
        super.starting(description);
        timeMap.put(description.hashCode(), System.currentTimeMillis());
    }

    @Override
    protected void finished(Description description) {
        long endTime = System.currentTimeMillis();
        Long startTime = timeMap.remove(description.hashCode());
        if (startTime != null) {
            System.err.println("Time: " + description.toString() + ": " + (endTime - startTime) + "ms");
        }
        super.finished(description);
    }
}
