package pl.hiber.testcase.runners.helpers;

/**
 * Created by 4ib3r on 2014-07-02.
 * Object for tests
 */
public class StatefulTestObj {
    public int number = 0;

    public StatefulTestObj(int number) {
        this.number = number;
    }

    public StatefulTestObj() {
    }

    public StatefulTestObj build() {
        number = 99;
        return this;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
