package pl.hiber.testcase.runners;

import org.junit.Test;
import org.junit.runner.RunWith;
import pl.hiber.testcase.annotations.TestAction;
import pl.hiber.testcase.annotations.TestCase;
import pl.hiber.testcase.runners.helpers.CartSystem;

import static junit.framework.Assert.assertEquals;

/**
 * Created by 4ib3r on 2014-07-03.
 */
@RunWith(TestCaseRunner.class)
public class BuyExample {

    private CartSystem cartSystem = new CartSystem();


    @TestCase
    public void oneProductAndRabat() {
        addRabat(5.0f);
        addProduct("example1");
        testCartSize(1);
        buyAction();
    }

    @TestCase
    public void twoTheSameProducts() {
        addProduct("example1");
        addProduct("example1");
        testCartSize(1);
        buyAction();
    }

    @TestCase
    public void threeProductsWithOneOther() {
        addProduct("example1");
        addProduct("example2");
        addProduct("example1");
        testCartSize(2);
        buyAction();
    }

    @TestAction
    public void addRabat(float value) {
        cartSystem.setRabat(value);
    }

    @TestAction
    public void addProduct(String ean) {
        cartSystem.addProduct(ean);
    }

    @TestAction
    public void buyAction() {
        cartSystem.finishCart();
    }

    @Test
    public void testCartSize(int expectedSize) {
        assertEquals(expectedSize, cartSystem.getProductsCount());
    }
}
