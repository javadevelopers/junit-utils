package pl.hiber.testcase.runners;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.hiber.testcase.annotations.TestCase;
import pl.hiber.testcase.annotations.TestObject;
import pl.hiber.testcase.runners.helpers.StatefulTestObj;

/**
 * Created by 4ib3r on 2014-07-03.
 */
@RunWith(TestCaseRunner.class)
public class TestCaseTestRealObjects {

    @TestObject
    public StatefulTestObj testObj = new StatefulTestObj(3);

    @TestCase
    public void testCase() {
        test(3);
        testObj.build().setNumber(5);
        test(5);
        testObj.setNumber(8);
        test(8);
    }

    @Test
    public void test(int i) {
        Assert.assertEquals(i, testObj.getNumber());
    }
}
