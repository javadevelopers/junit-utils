package pl.hiber.testcase.runners;

import org.junit.Test;
import pl.hiber.testcase.runners.helpers.StatefulTestObj;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by AdrianS on 23.02.15.
 */
public class speedTest {

    @Test
    public void testObjSpeed() {
        long t0, t1 = System.nanoTime();
        StatefulTestObj testObj = new StatefulTestObj();
        for (int i = 0; i < 1000000; i++) {
            testObj.number = i;
        }
        t0 = System.nanoTime();
        System.out.println(((t0 - t1)/1000000f) + "ms");
    }
    @Test
    public void testObjSpeedReflect() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        long t0, t1 = System.nanoTime();
        StatefulTestObj testObj = new StatefulTestObj();

        Field field = StatefulTestObj.class.getDeclaredField("number");
        //field.setAccessible(true);
        for (int i = 0; i < 1000000; i++) {
            field.set(testObj, i);
        }
        t0 = System.nanoTime();
        System.out.println(((t0 - t1)/1000000f) + "ms");
    }
}
