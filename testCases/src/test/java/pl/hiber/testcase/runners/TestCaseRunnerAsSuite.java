package pl.hiber.testcase.runners;

import org.junit.runner.RunWith;
import pl.hiber.testcase.annotations.TestCase;

/**
 * Created by 4ib3r on 2014-03-22.
 */
@RunWith(TestCaseRunner.class)
public class TestCaseRunnerAsSuite {
    @TestCase
    public void testCase(TestCaseRunnerOrderTest orderTest) {
        orderTest.setOrder(0);
        orderTest.test1();
        orderTest.test2();
        orderTest.test3();
        orderTest.test4();
    }
}
