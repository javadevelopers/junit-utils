package pl.hiber.testcase.runners;

import org.junit.Test;
import org.junit.runner.RunWith;
import pl.hiber.testcase.annotations.TestCase;

/**
 * Created by 4ib3r on 2014-07-03.
 */
@RunWith(TestCaseRunner.class)
public class ParametrizedTestCase {

    @TestCase(parameters = {"1","2","3"})
    public void testCase(Integer param) {
        test("i" + param);
    }

    @TestCase(parameters = {"a","b","c"})
    public void testCaseStrings(String param) {
        test("s" + param);
    }

    @TestCase(parameters = {"1.5","5.3357","0.7665654"})
    public void testCaseDouble(Double param) {
        test("d" + param);
    }

    @Test
    public void test(String param) {
        System.out.println("Test" + param);
    }
}
