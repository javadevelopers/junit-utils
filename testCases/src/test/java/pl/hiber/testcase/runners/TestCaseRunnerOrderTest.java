package pl.hiber.testcase.runners;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.hiber.testcase.annotations.AfterTestCase;
import pl.hiber.testcase.annotations.BeforeTestCase;
import pl.hiber.testcase.annotations.TestAction;
import pl.hiber.testcase.annotations.TestCase;

import java.util.Random;

/**
 * Created by 4ib3r on 2014-03-21.
 */
@RunWith(TestCaseRunner.class)
public class TestCaseRunnerOrderTest {

    public static int order = 0;
    
    private int step = 1;

    public int isStep() {
        return step;
    }

    @TestAction
    public void setStep(int step) {
        this.step = step;
    }

    @TestAction
    public void setOrder(int order) {
        TestCaseRunnerOrderTest.order = order;
    }

    @BeforeTestCase
    public void before() {
        TestCaseRunnerOrderTest.order = 0;
    }

    @AfterTestCase
    public void after() {
        if(step > 0)
            Assert.assertEquals(TestCaseRunnerOrderTest.order, 4);
        else
            Assert.assertEquals(TestCaseRunnerOrderTest.order, 1);
    }

    @TestCase
    public void testCase1(TestCaseRunnerOrderTest test) {
        test.test1();
        test.test2();
        test.test3();
        test.test4();
    }
    
    @TestCase
    public void testCase2(TestCaseRunnerOrderTest test) {
        test.test1();
        test.test2();
        test.test3();
        test.test4();
    }

    @TestCase
    public void testCase3(TestCaseRunnerOrderTest test) {
        test.test1();
        test.test2();
        test.test3();
        test.test4();
    }

    @TestCase
    public void testCase4(TestCaseRunnerOrderTest test) {
        test.setStep(-1);
        test.setOrder(5);
        test.test4();
        test.test3();
        test.test2();
        test.test1();
        
    }

    @Test
    public void test1() {
        TestCaseRunnerOrderTest.order += step;
        randomSleep();
        Assert.assertEquals(TestCaseRunnerOrderTest.order, 1);
    }

    @Test
    public void test2() {
        TestCaseRunnerOrderTest.order += step;
        randomSleep();
        Assert.assertEquals(TestCaseRunnerOrderTest.order, 2);
    }

    @Test
    public void test3() {
        TestCaseRunnerOrderTest.order += step;
        randomSleep();
        Assert.assertEquals(TestCaseRunnerOrderTest.order, 3);
    }

    @Test
    public void test4() {
        TestCaseRunnerOrderTest.order += step;
        randomSleep();
        Assert.assertEquals(TestCaseRunnerOrderTest.order, 4);
    }

    Random random = new Random();

    private void randomSleep() {
        try {
            int time = random.nextInt(200)*10;
            //System.out.println("Time: " + time);
            Thread.sleep(time);
        } catch (InterruptedException ignored) {
        }
    }
}
