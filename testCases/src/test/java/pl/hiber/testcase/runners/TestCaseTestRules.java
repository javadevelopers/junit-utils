package pl.hiber.testcase.runners;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.hiber.testcase.annotations.TestCase;
import pl.hiber.testcase.annotations.TestCaseRule;
import pl.hiber.testcase.annotations.TestObject;
import pl.hiber.testcase.runners.helpers.StatefulTestObj;
import pl.hiber.testcase.runners.helpers.TimerRule;

import static org.junit.Assert.assertEquals;

/**
 * Created by 4ib3r on 2014-07-03.
 */
@RunWith(TestCaseRunner.class)
public class TestCaseTestRules {

    @TestObject
    public StatefulTestObj testObj;

    @TestCaseRule
    public TimerRule testRule = new TimerRule();

    @Rule
    public TimerRule testMethodRule = new TimerRule();

    @TestCase
    public void testCase() {
        testObj.build().setNumber(5);
        test(5);
        testObj.setNumber(8);
        test(8);
    }

    @TestCase
    public void testCase2() {
        testObj.setNumber(10);
        test(10);
    }

    @Test
    public void test(int i) {
        System.out.println("test expected: " + i);
        assertEquals(i, testObj.getNumber());
    }
}
