package pl.hiber.testcase.runners;

import org.junit.Test;
import org.junit.runner.RunWith;
import pl.hiber.testcase.annotations.TestCase;
import pl.hiber.testcase.annotations.TestStep;
import pl.hiber.testcase.runners.helpers.TestObj;

/**
 * Created by 4ib3r on 2014-07-02.
 */
@RunWith(TestCaseRunner.class)
public class TestCaseTestMethodsOnSelf {

    @TestCase
    public void testCase(TestObj testObj) {
        testObj.action();
        test();
        testStep(2);
    }

    @TestCase
    public void secondTestCase(TestObj testObj) {
        testObj.action(5);
        test();
        testStep(88);
    }

    @Test
    public void test() {
        System.out.println("test");
    }

    @TestStep
    public void testStep(int i) {
        System.out.println("testStep: " + i);
    }
}
