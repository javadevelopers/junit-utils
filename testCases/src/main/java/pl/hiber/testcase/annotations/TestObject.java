package pl.hiber.testcase.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by 4ib3r on 2014-07-03.
 * Set field as object used in tests, object must have constructor without parameters.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface TestObject {
    boolean autoCreate() default true;
}
