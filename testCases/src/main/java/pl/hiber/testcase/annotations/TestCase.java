package pl.hiber.testcase.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created 2014 by 4ib3r
 * Annotation for mark test case. Marked method is invoked before tests for build test case structure.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface TestCase {
    /** Custom test case name */
    String value() default "";
    /** Parameters for test case, test case method can has only one primitive parameter for this */
    String[] parameters() default {};
}
