package pl.hiber.testcase.statements;

import org.junit.runners.model.Statement;

/**
 * Created by AdrianS on 2014-07-03.
 */
public class FrameworkMethodStatement extends Statement {
    FrameworkMethodAction child;

    public FrameworkMethodStatement(FrameworkMethodAction child) {
        this.child = child;
    }


    @Override
    public void evaluate() throws Throwable {
        child.invoke();
    }
}
