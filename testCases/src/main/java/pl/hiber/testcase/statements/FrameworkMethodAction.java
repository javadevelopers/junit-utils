package pl.hiber.testcase.statements;

import org.junit.runners.model.FrameworkMethod;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Created 2014 by 4ib3r
 * This is FrameworkMethod extended with testInstance and parameters.
 */
public class FrameworkMethodAction extends FrameworkMethod {

    /** Instance of test class */
	private Object testInstance;

    /** Parameters to use when test method is invoked */
	private Object[] parameters;

    private FrameworkMethodAction chainMethod = null;

    /** Mark test method as action, eg. setSomething(4). */
	private boolean isAction = false;

    /** Position method in test, to avoid repeating hash code */
	private int position;

	/**
	 * Returns a new {@code FrameworkMethod} for {@code method}
	 * @param method Method object
     * @param testInstance Instance of test class
     * @param isAction this method is action to set enviroment for test case
     * @param position position of method (for unique hash code)
	 */
	public FrameworkMethodAction(Method method, Object testInstance, int position, boolean isAction) {
		super(method);
		this.testInstance = testInstance;
		this.position = position;
		this.isAction = isAction;
	}

    /**  Returns a new {@code FrameworkMethod} for {@code method} (used for sub methods)
     * @param method Method object
     * */
    public FrameworkMethodAction(Method method) {
        super(method);
    }

    /** Return test instance */
	public Object getTestInstance() {
		return testInstance;
	}

    /** Invoke method with sub methods */
	public Object invoke() throws Throwable {
        Object res = this.invokeExplosively(testInstance, parameters);
        if (chainMethod != null) { //Invoke child method on this method result
            return chainMethod.invokeExplosively(res, chainMethod.parameters);
        }
		return res;
	}

	public boolean isAction() {
		return isAction;
	}


	public Object[] getParameters() {
		return parameters;
	}

	public void setParameters(Object[] parameters) {
		this.parameters = parameters;
	}

    /** Unique hash code for test method */
	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (testInstance != null ? testInstance.hashCode() : 0);
		result = 31 * result + (isAction ? 1 : 0);
		result = 31 * result + position;
		if (parameters != null)
			result = 31 * result + Arrays.hashCode(parameters);
		return result;
	}

    public FrameworkMethodAction getChainMethod() {
        return chainMethod;
    }

    public void setChainMethod(FrameworkMethodAction chainMethod) {
        this.chainMethod = chainMethod;
    }
}
