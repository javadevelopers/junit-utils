package pl.hiber.testcase.answers;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import pl.hiber.testcase.helpers.AnswerUtils;
import pl.hiber.testcase.statements.FrameworkMethodAction;

import java.io.Serializable;

/**
 * Created 2014 by 4ib3r
 * Custom mock answer, used for add sub methods to invoke.
 */
public final class SubAnswer implements Answer<Object>, Serializable {

    /** Parent method for answer */
    private FrameworkMethodAction parentMethod;

    /** Create {@code SubAnswer} for parent method
     * @param parentMethod Parent method for invoked */
    public SubAnswer(FrameworkMethodAction parentMethod) {
        this.parentMethod = parentMethod;
    }

    /**
     * When method in testCase method is invoked this method add this method and arguments to invoke list.
     * */
	@Override
	public Object answer(InvocationOnMock invocation) throws Throwable {
        FrameworkMethodAction fma = new FrameworkMethodAction(invocation.getMethod());
        fma.setParameters(invocation.getArguments());
        if (AnswerUtils.igonreMethod(fma)) {
            return null;
        }
        parentMethod.setChainMethod(fma);
        if (invocation.getMethod().getReturnType() != Void.TYPE) { //If method not void return result
            return Mockito.mock(invocation.getMethod().getReturnType(), new SubAnswer(fma));
        }
        return null;
	}
}
