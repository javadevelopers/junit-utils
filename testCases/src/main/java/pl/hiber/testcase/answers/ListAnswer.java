package pl.hiber.testcase.answers;

import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import pl.hiber.testcase.annotations.TestAction;
import pl.hiber.testcase.annotations.TestCase;
import pl.hiber.testcase.annotations.TestStep;
import pl.hiber.testcase.helpers.AnswerUtils;
import pl.hiber.testcase.statements.FrameworkMethodAction;

import java.io.Serializable;
import java.util.List;

/**
 * Created 2014 by 4ib3r
 * Custom mock answer, used for build list method for invoke in test case
 */
public final class ListAnswer implements Answer<Object>, Serializable {
	private final List<FrameworkMethodAction> frameworkMethods;
	private final Object bean;
	private final Class<?> testClass;

	public ListAnswer(List<FrameworkMethodAction> frameworkMethods, Object bean, Class<?> testClass) {
		this.frameworkMethods = frameworkMethods;
		this.testClass = testClass;
		this.bean = bean;
	}

    public ListAnswer(List<FrameworkMethodAction> frameworkMethods, Object testClassInstance) {
        this.frameworkMethods = frameworkMethods;
        this.testClass = testClassInstance.getClass();
        this.bean = testClassInstance;
    }

    /**
     * When method in testCase method is invoked this method add this method and arguments to invoke list.
     * */
	@Override
	public Object answer(InvocationOnMock invocation) throws Throwable {
		boolean isTest = invocation.getMethod().getAnnotation(Test.class) != null ||
                invocation.getMethod().getAnnotation(TestStep.class) != null;
		boolean isAction = invocation.getMethod().getAnnotation(TestAction.class) != null ||
				bean.getClass() != testClass;
		if (isTest || isAction) {
			FrameworkMethodAction fma = new FrameworkMethodAction(invocation.getMethod(), bean,
																  frameworkMethods.size(), !isTest);
			fma.setParameters(invocation.getArguments());
            if (AnswerUtils.igonreMethod(fma)) {
                return null;
            }
            frameworkMethods.add(fma);
            if (invocation.getMethod().getReturnType() != Void.TYPE) {
                return Mockito.mock(invocation.getMethod().getReturnType(), new SubAnswer(fma));
            }
            //If target method annotated TestCase invoke real method (for collect tests).
		} else if (invocation.getMethod().getAnnotation(TestCase.class) != null) {
            return invocation.callRealMethod();
        }
		return null;
	}
}
