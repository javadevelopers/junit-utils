package pl.hiber.testcase.helpers;

import org.junit.Rule;
import org.junit.internal.runners.statements.RunAfters;
import org.junit.internal.runners.statements.RunBefores;
import org.junit.rules.RunRules;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.junit.runners.model.TestClass;
import pl.hiber.testcase.annotations.AfterTestCase;
import pl.hiber.testcase.annotations.BeforeTestCase;
import pl.hiber.testcase.annotations.TestCaseRule;
import pl.hiber.testcase.statements.FrameworkMethodAction;

import java.util.List;

/**
 * Created by 4ib3r on 2014-07-03.
 * Helper for creating before, after and rules statements.
 */
public class RulesFactory {

    private Object testInstance;
    private TestClass testClass;

    public RulesFactory(Object testInstance, TestClass testClass) {
        this.testInstance = testInstance;
        this.testClass = testClass;
    }

    /**
     * Returns a {@link org.junit.runners.model.Statement}: apply all
     * static fields assignable to {@link org.junit.rules.TestRule}
     * annotated with {@link pl.hiber.testcase.annotations.TestCaseRule}.
     *
     * @param statement the base statement
     * @return a RunRules statement if any class-level {@link org.junit.Rule}s are
     *         found, or the base statement
     */
    public Statement withTestCasesRules(Statement statement, Object target, Description description) {
        List<TestRule> testCaseRules = testCaseRules(target);
        return testCaseRules.isEmpty() ? statement :
                new RunRules(statement, testCaseRules, description);
    }

    /**
     * @return the {@code ClassRule}s that can transform the block that runs
     *         each method in the tested class.
     */
    protected List<TestRule> testCaseRules(Object target) {
        List<TestRule> result = testClass.getAnnotatedMethodValues(target, TestCaseRule.class, TestRule.class);
        result.addAll(testClass.getAnnotatedFieldValues(target, TestCaseRule.class, TestRule.class));
        return result;
    }

    /** Create statement for after test case methods {@link pl.hiber.testcase.annotations.AfterTestCase} */
    public Statement withAfterTestCases(Statement statement) {
        List<FrameworkMethod> afters = testClass.getAnnotatedMethods(AfterTestCase.class);
        return afters.isEmpty() ? statement :
                new RunAfters(statement, afters, testInstance);
    }

    /** Create statement for before test case methods {@link pl.hiber.testcase.annotations.BeforeTestCase} */
    public Statement withBeforeTestCases(Statement statement) {
        List<FrameworkMethod> afters = testClass.getAnnotatedMethods(BeforeTestCase.class);
        return afters.isEmpty() ? statement :
                new RunBefores(statement, afters, testInstance);
    }


    public Statement withRules(FrameworkMethodAction method, Object target,
                               Statement statement, Description childDescription) {
        List<TestRule> testRules = getTestRules(target);
        Statement result = statement;
        //TODO check child.isAction()
        result = withMethodRules(method, testRules, target, result);
        result = withTestRules(method, testRules, result, childDescription);
        return result;
    }

    private Statement withMethodRules(FrameworkMethod method, List<TestRule> testRules,
                                      Object target, Statement result) {
        for (org.junit.rules.MethodRule each : getMethodRules(target)) {
            //noinspection SuspiciousMethodCalls
            if (!testRules.contains(each)) {
                result = each.apply(result, method, target);
            }
        }
        return result;
    }

    private List<org.junit.rules.MethodRule> getMethodRules(Object target) {
        return rules(target);
    }

    /**
     * @param target the test case instance
     * @return a list of MethodRules that should be applied when executing this
     *         test
     */
    protected List<org.junit.rules.MethodRule> rules(Object target) {
        return testClass.getAnnotatedFieldValues(target, Rule.class,
                org.junit.rules.MethodRule.class);
    }

    /**
     * @param statement The base statement
     * @return a RunRules statement if any class-level {@link Rule}s are
     *         found, or the base statement
     */
    private Statement withTestRules(FrameworkMethodAction method, List<TestRule> testRules,
                                    Statement statement, Description childDescription) {
        return testRules.isEmpty() ? statement :
                new RunRules(statement, testRules, childDescription);
    }

    /**
     * @param target the test case instance
     * @return a list of TestRules that should be applied when executing this
     *         test
     */
    protected List<TestRule> getTestRules(Object target) {
        List<TestRule> result = testClass.getAnnotatedMethodValues(target, Rule.class, TestRule.class);
        result.addAll(testClass.getAnnotatedFieldValues(target, Rule.class, TestRule.class));
        return result;
    }
}
