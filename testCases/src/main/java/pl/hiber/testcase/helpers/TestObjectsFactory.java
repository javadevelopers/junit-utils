package pl.hiber.testcase.helpers;

import java.util.Map;

/**
 * Created by AdrianS on 2014-07-03.
 */
public class TestObjectsFactory {

    /** Creation new instance of test class or pojo, or get it from list if it exist. */
    public static Object createOrGetTestInstance(Class<?> type, Map<String, Object> testInstances)
            throws IllegalAccessException, InstantiationException {
        Object testInstance = testInstances.get(type.getName());
        if (testInstance == null) {
            testInstance = type.newInstance();
            testInstances.put(type.getName(), testInstance);
        }
        return testInstance;
    }
}
