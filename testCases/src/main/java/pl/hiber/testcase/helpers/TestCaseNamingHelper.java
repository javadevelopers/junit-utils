package pl.hiber.testcase.helpers;

import org.junit.runner.Description;
import org.junit.runners.model.FrameworkMethod;
import pl.hiber.testcase.annotations.TestCase;
import pl.hiber.testcase.statements.FrameworkMethodAction;

/**
 * Created by 4ib3r on 2014-07-03.
 */
public class TestCaseNamingHelper {
    private Class<?> testJavaClass;
    private FrameworkMethod parent;
    private String name;
    private String param;

    public TestCaseNamingHelper(Class<?> testJavaClass, FrameworkMethod parent, String param) {
        this.testJavaClass = testJavaClass;
        this.parent = parent;
        this.param = param;
        this.name = buildName();
    }

    /** Return name for test case */
    public String getName() {
        return name + (param == null ? "" : " [" + param + "]");
    }

    /** Provide description for test method */
    public Description describeChild(FrameworkMethodAction child) {
        if (child.isAction())
            return describeAction(child);
        return describeTest(child);
    }

    /** Build name for test case */
    private String buildName() {
        TestCase testCase = parent.getAnnotation(TestCase.class);
        if (testCase != null && !"".equals(testCase.value()))
            return testCase.value();
        return parent.getName();
    }

    /** Create description for Test, use {@link #testName(pl.hiber.testcase.statements.FrameworkMethodAction)}
     * for generate description */
    private Description describeTest(FrameworkMethodAction method) {
        return Description.createTestDescription(name, testName(method), calcMethodHashCode(method));
    }

    /** Create description for TestAction, use {@link #actionName(pl.hiber.testcase.statements.FrameworkMethodAction)}
     * for generate description */
    private Description describeAction(FrameworkMethodAction method) {
        return Description.createTestDescription(name, actionName(method), calcMethodHashCode(method));
    }

    /** Calculate hash code for tests descriptions matching */
    private int calcMethodHashCode(FrameworkMethodAction method) {
        int result = 1;
        result = 31 * result + method.hashCode();
        result = 31 * result + parent.hashCode();
        return result;
    }

    /** Generate name for TestAction */
    private String actionName(FrameworkMethodAction frameworkMethod) {
        String action = "this";
        if (frameworkMethod.getTestInstance() == null) {
            action = "";
        } else if (frameworkMethod.getTestInstance().getClass() != testJavaClass)
            action = frameworkMethod.getTestInstance().getClass().getSimpleName();
        StringBuilder methodNameBuilder = new StringBuilder(frameworkMethod.getName());
        appendParameters(methodNameBuilder, frameworkMethod, true);
        if (frameworkMethod.getChainMethod() != null) { //Add submethods to name
            methodNameBuilder.append(actionName(frameworkMethod.getChainMethod()));
        }
        return String.format("%s.%s", action, methodNameBuilder.toString());
    }

    /** Append parameter values for action or test method */
    private void appendParameters(StringBuilder methodNameBuilder, FrameworkMethodAction frameworkMethod,
                                  boolean isAction) {
        if (isAction) {
            methodNameBuilder.append("(");
        }
        if(frameworkMethod.getParameters() != null && frameworkMethod.getParameters().length > 0) {
            if (!isAction) {
                methodNameBuilder.append(" [");
            }
            for (int i = 0; i < frameworkMethod.getParameters().length; i++) {
                Object o = frameworkMethod.getParameters()[i];
                if (i > 0) {
                    methodNameBuilder.append(", ");
                }
                if (o != null)
                    methodNameBuilder.append(o.toString());
                else
                    methodNameBuilder.append("null");
            }
            if (!isAction) {
                methodNameBuilder.append("]");
            }
        }
        if (isAction) {
            methodNameBuilder.append(")");
        }
    }

    /** Generate name for Test */
    private String testName(FrameworkMethodAction frameworkMethod) {
        StringBuilder stringBuilder = new StringBuilder();
        if (frameworkMethod.getTestInstance().getClass() != testJavaClass)
            stringBuilder.append(frameworkMethod.getTestInstance().getClass().getSimpleName()).append(".");
        stringBuilder.append(frameworkMethod.getName());
        appendParameters(stringBuilder, frameworkMethod, false);
        return stringBuilder.toString();
    }
}
