package pl.hiber.testcase.helpers;

import org.junit.runners.model.FrameworkMethod;

/**
 * Created by AdrianS on 2014-07-03.
 */
public class AnswerUtils {
    private AnswerUtils() {}

    /* Exclude finalize, toString, hash, getClass methods in test case. */
    public static boolean igonreMethod(FrameworkMethod method) {
        return "finalize".equals(method.getName()) ||
                "toString".equals(method.getName()) ||
                "hash".equals(method.getName()) ||
                "getClass".equals(method.getName());
    }
}
