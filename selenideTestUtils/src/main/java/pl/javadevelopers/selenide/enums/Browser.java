package pl.javadevelopers.selenide.enums;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by AdrianS on 2014-03-25.
 */
public enum Browser {
    FIREFOX(new FirefoxDriver());

    private WebDriver webDriver;

    Browser(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }
}
