package pl.javadevelopers.selenide.runners;

import org.junit.runner.RunWith;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import pl.hiber.testcase.runners.inner.InnerTestCaseRunner;
import pl.javadevelopers.selenide.runners.inner.SelenideInnerTestCaseRunner;

/**
 * Created by AdrianS on 2014-07-03.
 */
public class SelenideTestCaseRunner extends pl.hiber.testcase.runners.TestCaseRunner {
    /**
     * Constructor for run all test case's in test class
     *
     * @param type Class with test.
     */
    public SelenideTestCaseRunner(Class<?> type) throws InitializationError {
        super(type);
    }


    /** Create and prepare {@link pl.hiber.testcase.runners.inner.InnerTestCaseRunner} instance */
    @Override
    protected InnerTestCaseRunner prepareInnerTestCaseRunner(Class<?> type, FrameworkMethod frameworkMethod,
                                                             String param)
            throws InitializationError {
        SelenideInnerTestCaseRunner innerTestCaseRunner = new SelenideInnerTestCaseRunner(type,
                frameworkMethod,
                testInstance, otherTestInstances, param);
        innerTestCaseRunner.buildActionsList();
        return innerTestCaseRunner;
    }

    @Override
    protected void validateConstructorAndAnnotations(Class<?> type) throws InitializationError {
        if (type.getConstructors().length != 1) {
            throw new InitializationError("Test class " + type.getName() + " must have only one public constructor");
        }
        RunWith runWith = type.getAnnotation(RunWith.class);
        if (runWith != null && runWith.value() != SelenideTestCaseRunner.class) {
            throw new InitializationError("Test class " + type.getName() + " must be annotated with @RunWith(TestCaseRunner.class)");
        }
    }
}
