package pl.javadevelopers.selenide.runners.inner;

import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import pl.hiber.testcase.runners.inner.InnerTestCaseRunner;
import pl.javadevelopers.selenide.annotations.PageObject;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * Created by AdrianS on 2014-07-03.
 */
public class SelenideInnerTestCaseRunner extends InnerTestCaseRunner {

    /**
     * Constructor that's allow to create runner for simple test case
     *
     * @param testClass Class with tests
     * @param frameworkMethod Method metadata
     * @param testInstance Instance created from test class
     * @param otherTestInstances Instances of objects used in test.
     * @param param Additional parameter
     */
    public SelenideInnerTestCaseRunner(Class<?> testClass, FrameworkMethod frameworkMethod, Object testInstance,
                                       Map<String, Object> otherTestInstances,
                                       String param) throws InitializationError {
        super(testClass, frameworkMethod, testInstance, otherTestInstances, param);
    }

    @Override
    protected Object createTestObjectInstance(Field field) throws InstantiationException, IllegalAccessException {
        Object object = super.createTestObjectInstance(field);
        if (object != null) {
            return object;
        }
        PageObject pageObject = field.getAnnotation(PageObject.class);
        if (pageObject != null) {
            object = PageObjectProcessor.createInstance(field.getType());
            return object;
        }
        return null;
    }
}
