package pl.javadevelopers.selenide.runners.inner;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.internal.AssumptionViolatedException;
import org.junit.internal.runners.model.EachTestNotifier;
import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.ParentRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by AdrianS on 2014-03-25.
 */
public class WebDriverRunner extends ParentRunner<FrameworkMethod> {

    private WebDriver webDriver;

    private Object testInstance;

    /**
     * Constructs a new {@code ParentRunner} that will run {@code @TestClass}
     *
     * @param testClass class with tests
     * @param webDriver Web driver instance
     */
    public WebDriverRunner(Class<?> testClass, WebDriver webDriver) throws InitializationError {
        super(testClass);
        try {
            this.testInstance = getTestClass().getOnlyConstructor().newInstance();
            this.webDriver = webDriver;
        } catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
            throw new InitializationError(e);
        }
    }

    @Override
    protected List<FrameworkMethod> getChildren() {
        return getTestClass().getAnnotatedMethods(Test.class);
    }

    @Override
    protected Description describeChild(FrameworkMethod child) {
        return Description.createTestDescription(getTestClass().getJavaClass(),
                String.format("%s [%s]", child.getName(), webDriver.getClass().getSimpleName()));
    }

    @Override
    public void run(RunNotifier notifier) {
        this.webDriver.get("https://github.com/codeborne/selenide");
        super.run(notifier);
        this.webDriver.quit();
    }

    @Override
    protected void runChild(FrameworkMethod child, RunNotifier notifier) {
        EachTestNotifier eachNotifier = new EachTestNotifier(notifier, describeChild(child));
        eachNotifier.fireTestStarted();
        if (child.getAnnotation(Ignore.class) != null) {
            eachNotifier.fireTestIgnored();
        } else {
            try {
                child.invokeExplosively(testInstance);
            } catch (AssumptionViolatedException e) {
                eachNotifier.addFailedAssumption(e);
            } catch (Throwable e) {
                eachNotifier.addFailure(e);
            }
        }
        eachNotifier.fireTestFinished();
    }
}
