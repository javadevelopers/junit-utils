package pl.javadevelopers.selenide.runners.inner;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.openqa.selenium.By;
import pl.javadevelopers.selenide.annotations.Page;
import pl.javadevelopers.selenide.annotations.selectors.FindBy;

import java.lang.reflect.Method;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

/**
 * Created by AdrianS on 2014-07-03.
 */
public class PageObjectAnswer implements Answer {
    private Class<?> type;

    public PageObjectAnswer(Class<?> type) {
        this.type = type;
    }

    private By extractSelector(Method method) {
        FindBy findBy = method.getAnnotation(FindBy.class);
        if (findBy != null) {
            if (!"".equals(findBy.id())) {
                return By.id(findBy.id());
            }
            if (!"".equals(findBy.tag())) {
                return By.tagName(findBy.tag());
            }
            if (!"".equals(findBy.value())) {
                return By.cssSelector(findBy.value());
            }
        }
        return null;
    }

    @Override
    public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
        Method method = invocationOnMock.getMethod();
        Class<?> returnType = method.getReturnType();
        if (method.getName().equals("open")) {
            if (invocationOnMock.getArguments().length == 0) {
                Page page = type.getAnnotation(Page.class);
                if (page != null) {
                    System.out.println("open: " + page.value());
                    Selenide.open(page.value());
                }
            } else {
                Selenide.open((String) invocationOnMock.getArguments()[0]);
            }
            return null;
        }
        By selector = extractSelector(method);
        if (Boolean.class.isInstance(returnType)) {
            return $(selector).exists();
        }
        if (Integer.class.isInstance(returnType)) {
            return $$(selector).size();
        }
        if (String.class == returnType && method.getName().startsWith("get")) {
            return $(selector).val();
        }
        if (SelenideElement.class.isInstance(returnType)) {
            return $(selector);
        }
        if (ElementsCollection.class.isInstance(returnType)) {
            return $$(selector);
        }
        if (Void.TYPE.isAssignableFrom(returnType)) {
            if (method.getName().startsWith("click")) {
                $(selector).click();
            } else if (method.getName().startsWith("set")) {
                $(selector).setValue((String) invocationOnMock.getArguments()[0]);
            }
            return null;
        }
        return null;
    }
}
