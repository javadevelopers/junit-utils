package pl.javadevelopers.selenide.runners.inner;

import org.mockito.Mockito;

/**
 * Created by AdrianS on 2014-07-03.
 */
public class PageObjectProcessor {
    public static <T> T createInstance(Class<T> pageObjectInterface) {
        return Mockito.mock(pageObjectInterface, new PageObjectAnswer(pageObjectInterface));
    }
}
