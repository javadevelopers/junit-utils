package pl.javadevelopers.selenide.runners;

import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.ParentRunner;
import org.junit.runners.model.InitializationError;
import pl.javadevelopers.selenide.annotations.WebDrivers;
import pl.javadevelopers.selenide.runners.inner.WebDriverRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by AdrianS on 2014-03-24.
 */
public class SeleniumSuite extends ParentRunner<Runner> {
    private List<Runner> fChilds;

    /**
     * Constructs a new {@code ParentRunner} that will run {@code @TestClass}
     * @param testClass class with tests
     */
    public SeleniumSuite(Class<?> testClass) throws InitializationError {
        super(testClass);
        WebDrivers webDrivers = testClass.getAnnotation(WebDrivers.class);
        if (webDrivers != null) {
            pl.javadevelopers.selenide.annotations.WebDriver[] webDriversList = webDrivers.value();
            fChilds = new ArrayList<>(webDriversList.length);
            for (pl.javadevelopers.selenide.annotations.WebDriver wd : webDriversList) {
                fChilds.add(new WebDriverRunner(testClass, wd.value().getWebDriver()));
            }
        } else {
            fChilds = Collections.emptyList();
        }
    }

    @Override
    protected List<Runner> getChildren() {
        return fChilds;
    }

    @Override
    protected Description describeChild(Runner child) {
        return child.getDescription();
    }

    @Override
    protected void runChild(Runner child, RunNotifier notifier) {
        child.run(notifier);
    }
}
