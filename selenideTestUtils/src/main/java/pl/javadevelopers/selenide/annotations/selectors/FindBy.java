package pl.javadevelopers.selenide.annotations.selectors;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by AdrianS on 2014-07-03.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface FindBy {
    String value() default "";
    String id() default "";
    String tag() default "";
}
