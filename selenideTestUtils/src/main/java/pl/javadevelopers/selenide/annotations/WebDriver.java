package pl.javadevelopers.selenide.annotations;

import pl.javadevelopers.selenide.enums.Browser;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by 4ib3r on 2014-03-25.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface WebDriver {
    Browser value() default Browser.FIREFOX;
}
