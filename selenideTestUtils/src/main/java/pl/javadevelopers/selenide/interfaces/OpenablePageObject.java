package pl.javadevelopers.selenide.interfaces;

/**
 * Created by AdrianS on 2014-07-03.
 */
public interface OpenablePageObject {
    void open();
    void open(String page);
}
