package pl.javadevelopers.selenide.runners;

import org.junit.Test;
import org.junit.runner.RunWith;
import pl.javadevelopers.selenide.annotations.WebDriver;
import pl.javadevelopers.selenide.annotations.WebDrivers;
import pl.javadevelopers.selenide.enums.Browser;

/**
 * Created by AdrianS on 2014-03-25.
 */
@RunWith(SeleniumSuite.class)
@WebDrivers({
        @WebDriver(Browser.FIREFOX)
})
public class SeleniumSuiteTest {
    @Test
    public void test() {
        System.out.println("Test");
    }
}
