package pl.javadevelopers.selenide.runners.inner;

import com.codeborne.selenide.Configuration;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.hiber.testcase.annotations.TestCase;
import pl.javadevelopers.selenide.annotations.PageObject;
import pl.javadevelopers.selenide.runners.PageForm;
import pl.javadevelopers.selenide.runners.SelenideTestCaseRunner;

@RunWith(SelenideTestCaseRunner.class)
public class PageObjectProcessorTest {

    @BeforeClass
    public static void init() {
        Configuration.baseUrl = "file://T:/";
    }

    @PageObject
    public PageForm pageForm;

    @TestCase
    public void test() {
        pageForm.open();
        pageForm.clickTestButton();
        check();
    }

    @Test
    public void check() {

        Assert.assertTrue("123".equals(pageForm.getTestInput()));
    }
}