package pl.javadevelopers.selenide.runners;

import pl.javadevelopers.selenide.annotations.Page;
import pl.javadevelopers.selenide.annotations.selectors.FindBy;
import pl.javadevelopers.selenide.interfaces.OpenablePageObject;

/**
 * Created by AdrianS on 2014-07-03.
 */
@Page("test.html")
public interface PageForm extends OpenablePageObject {
    @FindBy("#testButton")
    void clickTestButton();

    @FindBy("#testInput")
    String getTestInput();
}
